<?php
/**
 * Created by PhpStorm.
 * User: blackcat636
 * Date: 31.12.2017
 * Time: 21:28
 */

namespace blackcat636\files\assets;


use yii\web\AssetBundle;


class FileInputWidgetAsset extends AssetBundle
{
    public $sourcePath = '@vendor/blackcat636/yii2-module-files/assets/';

    public $css = [
        'yii2-floor12-files.css',
    ];
    public $js = [
        'yii2-floor12-files.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'floor12\notification\NotificationAsset',
        'blackcat636\files\assets\CropperAsset',
        'blackcat636\files\assets\SimpleAjaxUploaderAsset',
    ];
}